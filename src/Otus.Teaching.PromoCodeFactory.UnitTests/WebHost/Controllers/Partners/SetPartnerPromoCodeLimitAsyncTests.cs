﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners

{
	public class SetPartnerPromoCodeLimitAsyncTests
    {
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_Return404()
        {
            //Arrange
            var partnerMock = new Mock<IRepository<Partner>>();
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Partner)null);
            var testPerson = new PartnersController(partnerMock.Object);

            //Act
            var result = await testPerson.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_Return400()
        {
            //Arrange
            var partnerMock = new Mock<IRepository<Partner>>();
            var partner = NewPartner(0,false);

            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testPerson = new PartnersController(partnerMock.Object);

            //Act
            var result = await testPerson.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.StatusCode.Should().Be(400);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_SetPartnerNumberIssuedCodesToZero()
        {
            //Arrange
            var activePromoCodeLimit = new PartnerPromoCodeLimit();

            var partner = NewPartner(100, true, new List<PartnerPromoCodeLimit>()
                {
                    activePromoCodeLimit
                });

            var partnerMock = new Mock<IRepository<Partner>>();
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testPerson = new PartnersController(partnerMock.Object);

            //Act
            var result = await testPerson.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
            activePromoCodeLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasNoActiveLimit_NotSetPartnerNumberIssuedCodesToZero()
        {
            //Arrange
            DateTime? oldLimit;
            var canceledPromoCodeLimit = new PartnerPromoCodeLimit()
            {
                CancelDate = DateTime.Now
            };
            oldLimit = canceledPromoCodeLimit.CancelDate;

            var partner = NewPartner(100, true, new List<PartnerPromoCodeLimit>()
                {
                    canceledPromoCodeLimit
                });

            var partnerMock = new Mock<IRepository<Partner>>();
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testPartner = new PartnersController(partnerMock.Object);

            //Act
            var result = await testPartner.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest() 
            {EndDate=new DateTime( 2025,1,1),
            Limit=300
            });

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(100);
            canceledPromoCodeLimit.CancelDate.Should().BeExactly(new TimeSpan(0)).Equals(oldLimit);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenNoLimit_BadRequestIsReturned()
        {
            //Arrange
            var partner = NewPartner(); 

            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testPartner = new PartnersController(partnerRepositoryMock.Object);
            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 0
            };
            //Act
            var result = await testPartner.SetPartnerPromoCodeLimitAsync(Guid.Empty, partnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.Value.Should().Be("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_SavedCorectly()
        {
            //Arrange
            var partner = NewPartner(0, true);

			var partnerMock = new Mock<IRepository<Partner>>();
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testee = new PartnersController(partnerMock.Object);
            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 5
            };

            //Act
            var result = await testee.SetPartnerPromoCodeLimitAsync(Guid.Empty, partnerPromoCodeLimitRequest);

            //Assert
            partnerMock.Verify(x => x.UpdateAsync(partner), Times.Once);
            partner.PartnerLimits.Count.Should().Be(1);
            partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).Limit.Should().Be(5);            
        }

        private Partner NewPartner(int Codes=0, bool isActive=true, List<PartnerPromoCodeLimit> partnerLimits= null)
		{
            return new Partner()
            {
                NumberIssuedPromoCodes = Codes,
                IsActive = isActive,
                PartnerLimits = partnerLimits == null ? new List<PartnerPromoCodeLimit>() : partnerLimits
            };
		}
    }
}